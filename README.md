# rest-card-game

Run with: mvn test spring-boot:run

Import [HAR](game-card.har) into REST client (insomnia, postman, ...) for availables service

Read [ca.logmein.card.game.BasicCardGameApplicationTest](src/test/java/ca/logmein/card/game/BasicCardGameApplicationTest.java) for documentation
