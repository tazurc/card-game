package ca.logmein.card.game;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackageClasses=BasicCardGameApplication.class)
public class BasicCardGameApplication {

	public static void main(String[] args) {
		SpringApplication.run(BasicCardGameApplication.class, args);
	}

}
