package ca.logmein.card.game;

import java.util.Random;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import ca.logmein.card.game.entity.Casino;

@Configuration
public class BasicCardGameConfiguration {

	@Bean
	public Random random() {
		return new Random();
	}
	
	@Bean
	public Casino casino() {
		return new Casino();
	}
	

}
