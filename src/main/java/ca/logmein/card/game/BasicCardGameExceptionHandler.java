package ca.logmein.card.game;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.NOT_FOUND;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import ca.logmein.card.game.service.EmtpyDeckException;
import ca.logmein.card.game.service.EntityNotFoundException;

@ControllerAdvice
public class BasicCardGameExceptionHandler {

	@ResponseStatus(value = NOT_FOUND, reason = "Whaaaa?")
	@ExceptionHandler({EntityNotFoundException.class,EmtpyDeckException.class})
	public void notFound() {
	}

	@ResponseStatus(value = BAD_REQUEST, reason = "Wait, no!")
	@ExceptionHandler({IllegalAccessException.class})
	public void badRequest() {
	}
	
}
