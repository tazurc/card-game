package ca.logmein.card.game.component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.stereotype.Component;

import ca.logmein.card.game.constant.Suit;
import ca.logmein.card.game.entity.Card;

@Component
public class DeckDispenser {

	private static final int CARDS_PER_DECK = 13;

	public Collection<? extends Card> get() {
		List<Card> deck = new ArrayList<>();
		
		for (Suit suit : Suit.values()) {
			for (int cardValue = 1; cardValue <= CARDS_PER_DECK; cardValue++) {
				Card card = new Card();
				card.setSuit(suit);
				card.setValue(cardValue);
				deck.add(card);
			}
		}
		
		return deck;
	}


}
