package ca.logmein.card.game.component;

import static java.util.stream.Collectors.toList;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import ca.logmein.card.game.dto.ReadGame;
import ca.logmein.card.game.dto.ReadPlayer;
import ca.logmein.card.game.entity.Game;
import ca.logmein.card.game.entity.Player;

@Component
public class Mapper {

	public ReadPlayer toDto(Player player) {
		ReadPlayer dto = new ReadPlayer();
		BeanUtils.copyProperties(player, dto);
		return dto;
	}

	public List<ReadGame> toDto(Collection<Game> games) {
		return games.stream().map(this::toDto).collect(toList());
		
	}

	public ReadGame toDto(Game game) {
		ReadGame dto = new ReadGame();
		BeanUtils.copyProperties(game, dto);
		dto.setPlayers(toDto(game.getPlayers()));
		return dto;
	}

	private List<ReadPlayer> toDto(List<Player> players) {
		return players.stream().map(this::toDto).collect(toList());
	}
	
}
