package ca.logmein.card.game.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ca.logmein.card.game.entity.Casino;

@Component
public class Sequence {
	private @Autowired Casino casino;

	public int getNextGameId() {
		return casino.getGames().size();
	}

	public int getNextPlayerId() {
		return casino.getPlayers().size();
	}

}
