package ca.logmein.card.game.component.shuffler;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Spliterator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.RequestScope;

import ca.logmein.card.game.entity.Card;

@Component
@RequestScope
public class HalfAndHalfShuffler implements Shuffler {

	
	private @Autowired Random random;
	private Spliterator<Card> firstHalfShoe;
	private Spliterator<Card> secondHalfShoe;
	private List<Card> shoeToShuffle;

	@Override
	public List<Card> shuffle(List<Card> shoe) {
		if (shoe == null) {
			throw new IllegalArgumentException("cards cannot be null");
		}
		if (shoe.size() == 1) {
			return shoe;
		}
		this.shoeToShuffle = shoe;

		//TODO reverse order to simulate a real half shuffle
		croupierCutShoeInHalf();

		List<Card> newShoe = croupierMergeBothHalf();
		
		return newShoe;
	}

	private List<Card> croupierMergeBothHalf() {
		List<Card> newShoe = new ArrayList<>();
		firstHalfShoe.forEachRemaining(card1 -> {
			boolean advanced = secondHalfShoe.tryAdvance(card2 -> {
				boolean card1First = random.nextBoolean();
				if (card1First) {
					newShoe.add(card1);
					newShoe.add(card2);
				} else {
					newShoe.add(card2);
					newShoe.add(card1);
				}
			});
			if(!advanced) {
				newShoe.add(card1);
			}
		});
		return newShoe;
	}

	private void croupierCutShoeInHalf() {
		firstHalfShoe = shoeToShuffle.spliterator();
		secondHalfShoe = firstHalfShoe.trySplit();
	}

}
