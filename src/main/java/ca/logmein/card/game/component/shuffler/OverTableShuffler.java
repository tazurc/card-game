package ca.logmein.card.game.component.shuffler;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ca.logmein.card.game.entity.Card;

@Component
public class OverTableShuffler implements Shuffler {

	private @Autowired Random random;

	@Override
	public List<Card> shuffle(List<Card> shoe) {
		if (shoe == null) {
			throw new IllegalArgumentException("cards cannot be null");
		}
		if (shoe.isEmpty()) {
			return shoe;
		}

		List<Card> newShoe = new ArrayList<>();
		
		while (shoe.size()>1) {
			int nextCardIndex = random.nextInt(shoe.size());
			newShoe.add(shoe.remove(nextCardIndex));
		}
		newShoe.add(shoe.remove(0));
		
		return newShoe;
	}

}
