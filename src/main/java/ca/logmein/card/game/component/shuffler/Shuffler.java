package ca.logmein.card.game.component.shuffler;

import java.util.List;

import ca.logmein.card.game.entity.Card;

public interface Shuffler {

	List<Card> shuffle(List<Card> cards);

}