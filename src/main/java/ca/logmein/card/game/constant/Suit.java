package ca.logmein.card.game.constant;

public enum Suit {
	HEART,SPADE,CLUB,DIAMOND
}
