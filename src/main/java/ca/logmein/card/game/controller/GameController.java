package ca.logmein.card.game.controller;

import static java.lang.String.format;
import static org.springframework.http.ResponseEntity.created;
import static org.springframework.http.ResponseEntity.ok;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

import ca.logmein.card.game.component.Mapper;
import ca.logmein.card.game.dto.CardCount;
import ca.logmein.card.game.dto.ReadGame;
import ca.logmein.card.game.dto.SuitCount;
import ca.logmein.card.game.entity.Game;
import ca.logmein.card.game.service.CasinoService;
import ca.logmein.card.game.service.CroupierService;
import ca.logmein.card.game.service.EntityNotFoundException;

@RestController
public class GameController {
	
	public static final String GAME_URL = "/games";

	private @Autowired CasinoService casino;
	private @Autowired CroupierService croupier;
	private @Autowired Mapper map;

	@PostMapping(GAME_URL)
	public ResponseEntity<Void> addGame() throws URISyntaxException {
		int id = casino.addGame();
		
		return created(new URI(format("%s/%s", GAME_URL,id))).build();
	}

	@PostMapping(GAME_URL+"/{gameId}/decks")
	public ResponseEntity<Void> addDeck(@PathVariable("gameId") Integer gameId) throws URISyntaxException, IllegalAccessException {
		Game game = casino.findGame(gameId).orElseThrow(IllegalAccessException::new);
		croupier.addDeckToGame(game);
		
		URI location = new URI(String.format("%s/%s/cards", GameController.GAME_URL,gameId));
		return created(location).build();
	}

	@GetMapping(GAME_URL)
	public ResponseEntity<List<ReadGame>> listGames() throws URISyntaxException {
		Collection<Game> games = casino.getGames();
		return ok(map.toDto(games));
	}
	
	@GetMapping(GAME_URL+"/{id}")
	public ResponseEntity<ReadGame> getGame(@PathVariable("id") Integer id) throws URISyntaxException, EntityNotFoundException, IllegalAccessException {
		Game game = casino.findGame(id).orElseThrow(EntityNotFoundException::new);
		return ok(map.toDto(game));
	}
	
	@DeleteMapping(GAME_URL+"/{id}")
	public ResponseEntity<ReadGame> deleteGame(@PathVariable("id") Integer id) throws URISyntaxException {
		casino.deleteGame(id);
		return ok().build();
	}
	
	@GetMapping(GAME_URL+"/{id}/suites")
	public ResponseEntity<List<SuitCount>> getSuitCount(@PathVariable("id") Integer id) throws URISyntaxException, IllegalAccessException, EntityNotFoundException {
		Game game = casino.findGame(id).orElseThrow(IllegalAccessException::new);
		
		//TODO map.toDto
		return ok(croupier.getSuiteCountForGame(game));
	}
	
	@GetMapping(GAME_URL+"/{id}/cards")
	public ResponseEntity<List<CardCount>> getCardCount(@PathVariable("id") Integer id) throws URISyntaxException, IllegalAccessException, EntityNotFoundException {
		Game game = casino.findGame(id).orElseThrow(IllegalAccessException::new);
		
		//TODO map.toDto
		List<CardCount> cardCountForGame = croupier.getCardCountForGame(game);
		return ok(cardCountForGame);
	}
	
	@PutMapping(GAME_URL+"/{id}/cards")
	public ResponseEntity<Void> shuffle(@PathVariable("id") Integer id) throws URISyntaxException, IllegalAccessException, EntityNotFoundException {
		Game game = casino.findGame(id).orElseThrow(IllegalAccessException::new);
		croupier.shuffle(game);
		return ok().build();
	}
	
}
