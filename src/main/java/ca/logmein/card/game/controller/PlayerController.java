package ca.logmein.card.game.controller;

import static java.lang.String.format;
import static org.springframework.http.ResponseEntity.created;
import static org.springframework.http.ResponseEntity.ok;

import java.net.URI;
import java.net.URISyntaxException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import ca.logmein.card.game.component.Mapper;
import ca.logmein.card.game.dto.ReadPlayer;
import ca.logmein.card.game.dto.WritePlayer;
import ca.logmein.card.game.entity.Game;
import ca.logmein.card.game.entity.Player;
import ca.logmein.card.game.service.CasinoService;
import ca.logmein.card.game.service.CroupierService;
import ca.logmein.card.game.service.EmtpyDeckException;
import ca.logmein.card.game.service.EntityNotFoundException;
import ca.logmein.card.game.service.PlayerService;

@RestController
public class PlayerController {
	
	public static final String PLAYER_URL = "/players";

	private @Autowired CasinoService casinoService;
	private @Autowired PlayerService playerService;
	private @Autowired CroupierService croupier;
	private @Autowired Mapper map;

	@PostMapping(PLAYER_URL)
	public ResponseEntity<Void> addPlayer(@RequestBody @Valid WritePlayer incomingPlayer) throws URISyntaxException, IllegalAccessException, EntityNotFoundException {
		Player player = playerService.checkIn(incomingPlayer.getName());
		Game game = casinoService.findGame(incomingPlayer.getGameId())
								.orElseThrow(IllegalAccessException::new);
		playerService.joinGame(player,game);
		
		URI location = new URI(format("%s/%s", PLAYER_URL, player.getId()));
		return created(location).build();
	}


	@GetMapping(PLAYER_URL+"/{id}")
	public ResponseEntity<ReadPlayer> getPlayer(@PathVariable("id") Integer id) throws URISyntaxException, EntityNotFoundException {
		Player player = playerService.find(id).orElseThrow(EntityNotFoundException::new);
		return ok(map.toDto(player));
	}
	
	@PostMapping(PLAYER_URL +"/{id}/cards")
	public ResponseEntity<Void> dealCard(@PathVariable("id") Integer id) throws URISyntaxException, EmtpyDeckException, EntityNotFoundException {
		Player player = playerService.find(id).orElseThrow(EntityNotFoundException::new);
		croupier.dealCard(player);
		URI location = new URI(format("%s/%s", PLAYER_URL,player.getId()));
		return created(location).build();
	}
	
	
}
