package ca.logmein.card.game.dto;

import ca.logmein.card.game.constant.Suit;

public class CardCount extends SuitCount {
	
	private int value;

	public CardCount(Suit suit, int value, Long count) {
		super(suit, count);
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}


}
