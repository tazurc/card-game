package ca.logmein.card.game.dto;

import java.util.ArrayList;
import java.util.List;

public class ReadGame {
	private int id;
	private List<ReadPlayer> players = new ArrayList<>();

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public List<ReadPlayer> getPlayers() {
		return players;
	}

	public void setPlayers(List<ReadPlayer> players) {
		this.players = players;
	}

}
