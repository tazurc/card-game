package ca.logmein.card.game.dto;

import java.util.ArrayList;
import java.util.List;

import ca.logmein.card.game.entity.Card;

public class ReadPlayer {

	private int id;
	private String name;
	private List<Card> cards = new ArrayList<>(); //TODO ReadCard dto
	private int total = 0;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public List<Card> getCards() {
		return cards;
	}

	public void setCards(List<Card> cards) {
		this.cards = cards;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
