package ca.logmein.card.game.dto;

import ca.logmein.card.game.constant.Suit;

public class SuitCount {

	private Suit suit;
	private Long count;
	
	public SuitCount(Suit suit, Long count) {
		this.suit = suit;
		this.count = count;
	}

	public Suit getSuit() {
		return suit;
	}

	public void setSuit(Suit suit) {
		this.suit = suit;
	}

	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}

}
