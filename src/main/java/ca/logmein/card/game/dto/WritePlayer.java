package ca.logmein.card.game.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class WritePlayer {
	
	private @NotBlank String name;
	private @NotNull Integer gameId;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getGameId() {
		return gameId;
	}

	public void setGameId(Integer gameId) {
		this.gameId = gameId;
	}

}
