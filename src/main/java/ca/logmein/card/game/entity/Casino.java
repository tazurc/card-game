package ca.logmein.card.game.entity;

import java.util.ArrayList;
import java.util.List;

public class Casino {
	private List<Game> games = new ArrayList<>();
	private List<Player> players = new ArrayList<>();

	public List<Game> getGames() {
		return games;
	}

	public void setGames(List<Game> games) {
		this.games = games;
	}

	public List<Player> getPlayers() {
		return players;
	}

	public void setPlayers(List<Player> players) {
		this.players = players;
	}

}
