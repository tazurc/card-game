package ca.logmein.card.game.entity;

import java.util.ArrayList;
import java.util.List;

public class Game {
	private int id;// TOOO unique interface
	private List<Player> players = new ArrayList<>();
	private List<Card> shoe = new ArrayList<>();
	private boolean active = true;

	public Game(int id) {
		this.id = id;
	}

	public List<Card> getShoe() {
		return shoe;
	}

	public void setShoe(List<Card> shoe) {
		this.shoe = shoe;
	}

	public List<Player> getPlayers() {
		return players;
	}

	public void setPlayers(List<Player> players) {
		this.players = players;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

}
