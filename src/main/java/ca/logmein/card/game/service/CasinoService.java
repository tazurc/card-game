package ca.logmein.card.game.service;

import static java.util.Comparator.comparing;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static java.util.stream.Collectors.toList;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ca.logmein.card.game.component.Sequence;
import ca.logmein.card.game.entity.Casino;
import ca.logmein.card.game.entity.Game;
import ca.logmein.card.game.entity.Player;

@Service
public class CasinoService {

	private @Autowired Casino casino;
	private @Autowired Sequence sequence;

	public int addGame() {
		Game game = new Game(sequence.getNextGameId());
		casino.getGames().add(game);
		return game.getId();
	}

	public Collection<Game> getGames() {
		List<Game> activeGames = casino.getGames().stream().filter(Game::isActive).collect(toList());
		return Collections.unmodifiableCollection(activeGames);
	}

	public void deleteGame(Integer id) {
		Game gameToDelete = casino.getGames().get(id);
		gameToDelete.setActive(false);
	}

	public Optional<Game> findGame(Integer id) throws IllegalAccessException {
		if (id < casino.getGames().size()) {
			Game game = casino.getGames().get(id);
			if (game.isActive()) {
				// TODO find someplace better to do this! auto sort list? sorted access list?
				// sort when card is dealt?
				game.getPlayers().sort(comparing(Player::getTotal).reversed());
				return of(game);
			}
			throw new IllegalAccessException();
		}
		return empty();
	}

}
