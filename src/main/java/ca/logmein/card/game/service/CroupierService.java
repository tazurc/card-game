package ca.logmein.card.game.service;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ca.logmein.card.game.component.DeckDispenser;
import ca.logmein.card.game.component.shuffler.Shuffler;
import ca.logmein.card.game.constant.Suit;
import ca.logmein.card.game.dto.CardCount;
import ca.logmein.card.game.dto.SuitCount;
import ca.logmein.card.game.entity.Card;
import ca.logmein.card.game.entity.Game;
import ca.logmein.card.game.entity.Player;

@Service
public class CroupierService {

	private static final Logger LOGGER = LoggerFactory.getLogger(CroupierService.class);
	private @Autowired List<Shuffler> shufflers = new ArrayList<>();
	private @Autowired Random random;
	private @Autowired DeckDispenser deckDispenser;

	public void addDeckToGame(Game game) {
		game.getShoe().addAll(deckDispenser.get());
	}
	
	public void dealCard(Player player) throws EmtpyDeckException {
		Game gameToUpdate = player.getGame();

		if (gameToUpdate.getShoe().isEmpty()) {
			throw new EmtpyDeckException();
		}
		
		Card dealtCard = gameToUpdate.getShoe().remove(0);
		player.getCards().add(dealtCard);
		player.setTotal(player.getTotal() + dealtCard.getValue());
	}

	public List<SuitCount> getSuiteCountForGame(Game game) {
		Map<Suit, Long> countPerSuit = game.getShoe().stream().collect(
                groupingBy(Card::getSuit, counting()));

		for (Suit suit : Suit.values()) {
			countPerSuit.putIfAbsent(suit, 0l);
		}
		return countPerSuit.entrySet().stream()
			.map(entry->new SuitCount(entry.getKey(),entry.getValue()))
			.collect(Collectors.toList());
	}

	public List<CardCount> getCardCountForGame(Game game) {
		List<CardCount> cardCounts = new ArrayList<>();
		for (Suit suit : Suit.values()) {
			AtomicInteger cardValue = new AtomicInteger(0);
			while(cardValue.getAndIncrement() < 13) {
				Long count = game.getShoe().stream()
					.filter(card->card.getSuit() == suit)
					.filter(card->card.getValue() == cardValue.get())
					.collect(counting());
				CardCount cardCount = new CardCount(suit,cardValue.get(),count);
				cardCounts.add(cardCount);
			}
		}
		
		cardCounts.sort(comparing(SuitCount::getSuit));
		return cardCounts;
		
	}

	public void shuffle(Game game) {
		LOGGER.info("cards before shuffle:");
		LOGGER.info(game.getShoe().toString());

		Shuffler shuffler = pickShuffler();
		LOGGER.info("shuffling with: {}",shuffler);
		
		List<Card> newShoe = shuffler.shuffle(game.getShoe());
		game.setShoe(newShoe);

		LOGGER.info("cards after shuffle:");
		LOGGER.info(game.getShoe().toString());
	}

	private Shuffler pickShuffler() {
		int shufflerIndex = random.nextInt(shufflers.size());
		Shuffler shuffler = shufflers.get(shufflerIndex);
		return shuffler;
	}

}
