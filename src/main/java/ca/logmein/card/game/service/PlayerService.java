package ca.logmein.card.game.service;

import static java.util.Optional.empty;
import static java.util.Optional.of;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ca.logmein.card.game.component.Sequence;
import ca.logmein.card.game.entity.Casino;
import ca.logmein.card.game.entity.Game;
import ca.logmein.card.game.entity.Player;

@Service
public class PlayerService {

	private @Autowired Casino casino;
	private @Autowired Sequence sequence;

	public void joinGame(Player player, Game game) throws IllegalAccessException {
		game.getPlayers().add(player);
		player.setGame(game);
	}

	public Optional<Player> find(Integer id) {
		if (id < casino.getPlayers().size()) {
			return of(casino.getPlayers().get(id));
		}
		return empty();
	}

	public Player checkIn(String name) {
		Player player = new Player(sequence.getNextPlayerId());
		player.setName(name);
		casino.getPlayers().add(player);
		return player;
	}


}
