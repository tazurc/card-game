package ca.logmein.card.game;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import ca.logmein.card.game.dto.WritePlayer;
import ca.logmein.card.game.entity.Casino;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureRestDocs
public class BasicCardGameApplicationTest {

	private static final int AS = 1;
	private static final int JACK = 11;
	private static final int QUEEN = 12;
	private static final int KING = 13;
	private static MockMvc mvc;
	
	private @Autowired WebApplicationContext webApplicationContext;
	private @Autowired Casino casino;
	private @Autowired ObjectMapper mapper;

	private int gameCounter;

	@Before
	public void setUp() throws Exception {
		mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
		resetCasino();
		gameCounter = 0;
	}

	private void resetCasino() throws Exception {
		casino.setGames(new ArrayList<>());
		casino.setPlayers(new ArrayList<>());
	}


	@Test
	public void addGame() throws Exception {
		casinoAddGame();

		expect("/games").returns("[{id:0}]");
	}

	@Test
	public void AddTwoGames() throws Exception {
		casinoAddGame();
		casinoAddGame();

		expect("/games").returns("[{id:0},{id:1}]");
	}

	@Test
	public void addDeckToGame() throws Exception {
		casinoAddGame();
		inGame(0).croupier().addDeck();
		
		expect("/games/0/suites").returns(""
				+ "["
				+ "	{suit:HEART,count:13},"
				+ "	{suit:DIAMOND,count:13},"
				+ "	{suit:CLUB,count:13},"
				+ "	{suit:SPADE,count:13}"
				+ "]");
	}

	@Test
	public void addDeckToTwoGames() throws Exception {
		casinoAddGame();
		casinoAddGame();
		inGame(1).croupier().addDeck();
		inGame(1).croupier().addDeck();
		
		expect("/games/1/suites").returns(""
				+ "["
				+ "	{suit:HEART,count:26},"
				+ "	{suit:DIAMOND,count:26},"
				+ "	{suit:CLUB,count:26},"
				+ "	{suit:SPADE,count:26}"
				+ "]");
	}
	
	@Test
	public void addPlayers() throws Exception {
		casinoAddGame();
		casinoAddGame();
		inGame(0).player(0, "player1").join();
		inGame(1).player(1, "player2").join();
		inGame(0).player(2, "player3").join();
		
		expect("/games/0").returns("{id:0,players:[{id:0,name:player1},{id:2,name:player3}]}");
		expect("/games/1").returns("{id:1,players:[{id:1,name:player2}]}");
	}
	
	@Test
	public void noShuffle_dealCards() throws Exception {
		casinoAddGame();
		inGame(0).player(0, "player1").join();
		inGame(0).player(1, "player1").join();
		inGame(0).croupier().addDeck();
		inGame(0).player(0, "player1").askCard(1);
		inGame(0).player(1, "player1").askCard(2);
		expect("/players/0")
			.returns(""
				+ "{"
				+ "	id:0,"
				+ "	cards:["
				+ "		{suit:HEART,value:" + AS + "}"
				+ "	]"
				+ "}");
		expect("/players/1")
			.returns(""
				+ "{"
				+ "	id:1,"
				+ "	cards:["
				+ "		{suit:HEART,value:2},"
				+ "		{suit:HEART,value:3}"
				+ "	]"
				+ "}");
	}
	
	@Test
	public void emptyDeck() throws Exception {
		casinoAddGame();
		inGame(0).player(0, "player1").join();
		inGame(0).croupier().addDeck();
		inGame(0).player(0, "player1").askCard(52);

		inGame(0).player(0, "player1").askCardButDeckIsEmpty();
	}

	@Test
	public void noShuffle_playersInGame() throws Exception {
		casinoAddGame();
		inGame(0).player(0, "player1").join();
		inGame(0).player(1, "player1").join();
		inGame(0).croupier().addDeck();
		inGame(0).player(0, "player1").askCard(3);
		inGame(0).player(1, "player1").askCard(2);
		
		expect("/games/0")
			.returns(""
					+ "{"
					+ "	id:0,"
					+ "	players:["
					+ "		{"
					+ "			id:1,"
					+ "			total:9"
					+ "		},"
					+ "		{"
					+ "			id:0,"
					+ "			total:6"
					+ "		}"
					+ "	]"
					+ "}")
			.jsonPath("$.players[0].id", result -> result.isEqualTo(1))
			.jsonPath("$.players[1].id", result -> result.isEqualTo(0));
	}
	
	@Test
	public void noShuffle_countCardsLeftPerSuite() throws Exception {
		casinoAddGame();
		inGame(0).croupier().addDeck();
		inGame(0).player(0, "player1").join();
		inGame(0).player(0, "player1").askCard(20);
		
		expect("/games/0/suites").returns(""
				+ "["
				+ "	{suit:HEART,count:0},"
				+ "	{suit:SPADE,count:6},"
				+ "	{suit:DIAMOND,count:13},"
				+ "	{suit:CLUB,count:13}"
				+ "]");
	}

	@Test
	public void noShuffle_countEachCardsRemainingInGame() throws Exception {
		casinoAddGame();
		inGame(0).croupier().addDeck();
		inGame(0).croupier().addDeck();
		inGame(0).croupier().addDeck();
		inGame(0).player(0, "player1").join();
		inGame(0).player(0, "player1").askCard(55);
		
		expect("/games/0/cards").strict().returns(""
				+ "["
				+ "	{suit:HEART, value: "+AS+",count:1},"
				+ "	{suit:HEART, value: 2,count:1},"
				+ "	{suit:HEART, value: 3,count:1},"
				+ "	{suit:HEART, value: 4,count:2},"
				+ "	{suit:HEART, value: 5,count:2},"
				+ "	{suit:HEART, value: 6,count:2},"
				+ "	{suit:HEART, value: 7,count:2},"
				+ "	{suit:HEART, value: 8,count:2},"
				+ "	{suit:HEART, value: 9,count:2},"
				+ "	{suit:HEART, value: 10,count:2},"
				+ "	{suit:HEART, value: "+JACK+",count:2},"
				+ "	{suit:HEART, value: "+QUEEN+",count:2},"
				+ "	{suit:HEART, value: "+KING+",count:2},"
				+ "	{suit:SPADE, value: "+AS+",count:2},"
				+ "	{suit:SPADE, value: 2,count:2},"
				+ "	{suit:SPADE, value: 3,count:2},"
				+ "	{suit:SPADE, value: 4,count:2},"
				+ "	{suit:SPADE, value: 5,count:2},"
				+ "	{suit:SPADE, value: 6,count:2},"
				+ "	{suit:SPADE, value: 7,count:2},"
				+ "	{suit:SPADE, value: 8,count:2},"
				+ "	{suit:SPADE, value: 9,count:2},"
				+ "	{suit:SPADE, value: 10,count:2},"
				+ "	{suit:SPADE, value: "+JACK+",count:2},"
				+ "	{suit:SPADE, value: "+QUEEN+",count:2},"
				+ "	{suit:SPADE, value: "+KING+",count:2},"
				+ "	{suit:CLUB, value: "+AS+",count:2},"
				+ "	{suit:CLUB, value: 2,count:2},"
				+ "	{suit:CLUB, value: 3,count:2},"
				+ "	{suit:CLUB, value: 4,count:2},"
				+ "	{suit:CLUB, value: 5,count:2},"
				+ "	{suit:CLUB, value: 6,count:2},"
				+ "	{suit:CLUB, value: 7,count:2},"
				+ "	{suit:CLUB, value: 8,count:2},"
				+ "	{suit:CLUB, value: 9,count:2},"
				+ "	{suit:CLUB, value: 10,count:2},"
				+ "	{suit:CLUB, value: "+JACK+",count:2},"
				+ "	{suit:CLUB, value: "+QUEEN+",count:2},"
				+ "	{suit:CLUB, value: "+KING+",count:2},"
				+ "	{suit:DIAMOND, value: "+AS+",count:2},"
				+ "	{suit:DIAMOND, value: 2,count:2},"
				+ "	{suit:DIAMOND, value: 3,count:2},"
				+ "	{suit:DIAMOND, value: 4,count:2},"
				+ "	{suit:DIAMOND, value: 5,count:2},"
				+ "	{suit:DIAMOND, value: 6,count:2},"
				+ "	{suit:DIAMOND, value: 7,count:2},"
				+ "	{suit:DIAMOND, value: 8,count:2},"
				+ "	{suit:DIAMOND, value: 9,count:2},"
				+ "	{suit:DIAMOND, value: 10,count:2},"
				+ "	{suit:DIAMOND, value: "+JACK+",count:2},"
				+ "	{suit:DIAMOND, value: "+QUEEN+",count:2},"
				+ "	{suit:DIAMOND, value: "+KING+",count:2}"
				+ "]");
	}
	@Test
	public void noShuffle_countEachCardsRemainingInGame_mustBe52Cards() throws Exception {
		casinoAddGame();
		inGame(0).croupier().addDeck();
		inGame(0).player(0, "player1").join();
		inGame(0).player(0, "player1").askCard(14);
		
		expect("/games/0/cards").strict().returns(""
				+ "["
				+ "	{suit:HEART, value: "+AS+",count:0},"
				+ "	{suit:HEART, value: 2,count:0},"
				+ "	{suit:HEART, value: 3,count:0},"
				+ "	{suit:HEART, value: 4,count:0},"
				+ "	{suit:HEART, value: 5,count:0},"
				+ "	{suit:HEART, value: 6,count:0},"
				+ "	{suit:HEART, value: 7,count:0},"
				+ "	{suit:HEART, value: 8,count:0},"
				+ "	{suit:HEART, value: 9,count:0},"
				+ "	{suit:HEART, value: 10,count:0},"
				+ "	{suit:HEART, value: "+JACK+",count:0},"
				+ "	{suit:HEART, value: "+QUEEN+",count:0},"
				+ "	{suit:HEART, value: "+KING+",count:0},"
				+ "	{suit:SPADE, value: "+AS+",count:0},"
				+ "	{suit:SPADE, value: 2,count:1},"
				+ "	{suit:SPADE, value: 3,count:1},"
				+ "	{suit:SPADE, value: 4,count:1},"
				+ "	{suit:SPADE, value: 5,count:1},"
				+ "	{suit:SPADE, value: 6,count:1},"
				+ "	{suit:SPADE, value: 7,count:1},"
				+ "	{suit:SPADE, value: 8,count:1},"
				+ "	{suit:SPADE, value: 9,count:1},"
				+ "	{suit:SPADE, value: 10,count:1},"
				+ "	{suit:SPADE, value: "+JACK+",count:1},"
				+ "	{suit:SPADE, value: "+QUEEN+",count:1},"
				+ "	{suit:SPADE, value: "+KING+",count:1},"
				+ "	{suit:CLUB, value: "+AS+",count:1},"
				+ "	{suit:CLUB, value: 2,count:1},"
				+ "	{suit:CLUB, value: 3,count:1},"
				+ "	{suit:CLUB, value: 4,count:1},"
				+ "	{suit:CLUB, value: 5,count:1},"
				+ "	{suit:CLUB, value: 6,count:1},"
				+ "	{suit:CLUB, value: 7,count:1},"
				+ "	{suit:CLUB, value: 8,count:1},"
				+ "	{suit:CLUB, value: 9,count:1},"
				+ "	{suit:CLUB, value: 10,count:1},"
				+ "	{suit:CLUB, value: "+JACK+",count:1},"
				+ "	{suit:CLUB, value: "+QUEEN+",count:1},"
				+ "	{suit:CLUB, value: "+KING+",count:1},"
				+ "	{suit:DIAMOND, value: "+AS+",count:1},"
				+ "	{suit:DIAMOND, value: 2,count:1},"
				+ "	{suit:DIAMOND, value: 3,count:1},"
				+ "	{suit:DIAMOND, value: 4,count:1},"
				+ "	{suit:DIAMOND, value: 5,count:1},"
				+ "	{suit:DIAMOND, value: 6,count:1},"
				+ "	{suit:DIAMOND, value: 7,count:1},"
				+ "	{suit:DIAMOND, value: 8,count:1},"
				+ "	{suit:DIAMOND, value: 9,count:1},"
				+ "	{suit:DIAMOND, value: 10,count:1},"
				+ "	{suit:DIAMOND, value: "+JACK+",count:1},"
				+ "	{suit:DIAMOND, value: "+QUEEN+",count:1},"
				+ "	{suit:DIAMOND, value: "+KING+",count:1}"
				+ "]");
	}

	@Test
	public void shuffle() throws Exception {
		casinoAddGame();
		inGame(0).croupier().addDeck();
		inGame(0).player(0, "player1").join();
		inGame(0).croupier().shuffle().andExpect(status().isOk());
		
		//TODO mock Random and force shuffle to not come to initial state
		//	and askCard(1) -> expect card is not AS HEART
		expect("/games/0/suites").returns(""
				+ "["
				+ "	{suit:HEART,count:13},"
				+ "	{suit:SPADE,count:13},"
				+ "	{suit:DIAMOND,count:13},"
				+ "	{suit:CLUB,count:13}"
				+ "]");
	}

	@Test
	public void addPlayerWithoutName() throws Exception {
		casinoAddGame();
		inGame(0).player(0, "").cannotJoin();
	}
	
	@Test
	public void addPlayerWithoutGameId() throws Exception {
		casinoAddGame();
		inGame(null).player(0, "player1").cannotJoin();
	}
	
	@Test
	public void deleteGame() throws Exception {
		casinoAddGame();
		casinoDeleteGame(0);
		
		expectBadRequest("/games/0");
	}
	
	@Test
	public void deleteGame_notInlist() throws Exception {
		casinoAddGame();
		casinoAddGame();
		casinoDeleteGame(0);
		
		expect("/games").returns("[{id:1}]");
	}
	
	@Test
	public void deleteGame_playerCannotJoin() throws Exception {
		casinoAddGame();
		casinoDeleteGame(0);
		
		inGame(0).player(0, "distracted player").cannotJoin();
		
	}
	
	@Test
	public void deleteGame_shuffle() throws Exception {
		casinoAddGame();
		inGame(0).croupier().addDeck();
		casinoDeleteGame(0);
		
		inGame(0).croupier().shuffle().andExpect(status().isBadRequest());
		
	}
	
	//TODO player join non existing game 
	//TODO croupier add deck to non existing game
	//TODO add deck to a game if first card was dealt?
	//TODO add player in two game -> operation not permitted
	//TODO delete player from game and add it to another game -> cards are emptied


	private void expectBadRequest(String url) throws Exception {
		mvc.perform(get(url).accept(APPLICATION_JSON)).andExpect(status().isBadRequest());
	}

	private void casinoDeleteGame(int gameId) throws Exception {
		mvc.perform(delete("/games/"+gameId).accept(APPLICATION_JSON))//
		.andExpect(status().isOk());
	}

	public GameAction inGame(Integer gameId) {
		return new GameAction(gameId);
	}

	private class PlayerAction{
		private int expectedPlayerId;
		private WritePlayer player;

		public PlayerAction(Integer gameId, int playerId, String playerName) {
			this.expectedPlayerId = playerId;
			player = new WritePlayer();
			player.setName(playerName);
			player.setGameId(gameId);
		}

		public void cannotJoin() throws Exception {
			doJoin().andExpect(status().isBadRequest());
		}

		public void join() throws Exception {
			doJoin()
				.andExpect(status().isCreated())
				.andExpect(header().string("location", "/players/"+expectedPlayerId));
		}

		public ResultActions doJoin() throws Exception {
			String payload = mapper.writer().writeValueAsString(player);
			return mvc.perform(post("/players").contentType(MediaType.APPLICATION_JSON).content(payload));
		}
		
		public void askCard(int count) throws Exception {
			while (count-- > 0) {
				askCard()
				.andExpect(status().isCreated());
			}
		}

		private ResultActions askCard() throws Exception {
			return mvc.perform(post("/players/"+expectedPlayerId+"/cards"));
		}

		public void askCardButDeckIsEmpty() throws Exception {
			askCard().andExpect(status().isNotFound());
		}

}

	private class CroupierAction{
		private int gameId;

		public CroupierAction(Integer gameId) {
			this.gameId = gameId;
		}

		public ResultActions shuffle() throws Exception {
			return mvc.perform(put("/games/" + gameId+"/cards").accept(MediaType.APPLICATION_JSON));
		}

		public void addDeck() throws Exception {
			mvc.perform(post("/games/" + gameId+"/decks").accept(MediaType.APPLICATION_JSON))//
			.andExpect(status().isCreated())
			.andExpect(header().string("location", "/games/" + gameId+"/cards"));
		}

	}

	private class GameAction{
		private Integer gameId;

		public GameAction(Integer gameId) {
			this.gameId = gameId;
		}

		public CroupierAction croupier() {
			return new CroupierAction(gameId);
		}

		public PlayerAction player(int id, String name) {
			return new PlayerAction(gameId,id,name);
		}
	}


	private ExpectBuilder expect(String url) throws Exception {
		return new ExpectBuilder(mvc.perform(get(url).accept(APPLICATION_JSON)));
	}

	private void casinoAddGame() throws Exception {
		mvc.perform(post("/games").accept(APPLICATION_JSON))//
				.andExpect(status().isCreated())//
				.andExpect(header().string("location", "/games/"+gameCounter++));
	}

}
