package ca.logmein.card.game;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.UnsupportedEncodingException;
import java.util.function.Consumer;

import org.assertj.core.api.AbstractIntegerAssert;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.test.web.servlet.ResultActions;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ExpectBuilder {

	private String actualJson;
	private boolean strict = false;

	public ExpectBuilder(ResultActions perform) throws UnsupportedEncodingException, Exception {
		actualJson = perform.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
		ObjectMapper objectMapper = new ObjectMapper();
		JsonNode root = objectMapper.readTree(actualJson);
		System.out.println(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(root));;
	}

	public ExpectBuilder returns(String json) throws Exception {
		JSONAssert.assertEquals(json, actualJson, strict);
		return this;
	}

	public  ExpectBuilder jsonPath(String path,Consumer<AbstractIntegerAssert<?>> assertion) throws Exception {
		Integer actual = com.jayway.jsonpath.JsonPath.parse(actualJson).read(path);
		assertion.accept(assertThat(actual));
		return this;
	}

	public ExpectBuilder strict() {
		this.strict = true;
		return this;
	}

}
