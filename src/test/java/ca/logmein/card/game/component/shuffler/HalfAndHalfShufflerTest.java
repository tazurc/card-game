package ca.logmein.card.game.component.shuffler;

import static ca.logmein.card.game.constant.Suit.CLUB;
import static ca.logmein.card.game.constant.Suit.DIAMOND;
import static ca.logmein.card.game.constant.Suit.HEART;
import static ca.logmein.card.game.constant.Suit.SPADE;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.BDDMockito.given;

import java.util.List;

import org.assertj.core.api.ThrowableAssert.ThrowingCallable;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import ca.logmein.card.game.constant.Suit;
import ca.logmein.card.game.entity.Card;

@RunWith(MockitoJUnitRunner.class)
public class HalfAndHalfShufflerTest extends ShufflerTest {

	private static final boolean CARD1_FIRST = true;
	private static final boolean CARD2_FIRST = false;
	private @InjectMocks HalfAndHalfShuffler underTest;

	@Test
	public void cardsIsNull() {
		// given
		shoe = null;

		// when
		ThrowingCallable shuffle = () -> underTest.shuffle(shoe);

		// then
		assertThatThrownBy(shuffle)
				.isInstanceOf(IllegalArgumentException.class)
				.hasMessage("cards cannot be null");
	}
	
	@Test
	public void shuffle_halfAndHalf_4cards() {
		// given
		shoeContains(
				card(HEART, 3),
				card(DIAMOND, 6),
				card(SPADE, 1),
				card(CLUB, 10));
		given(random.nextBoolean())
			.willReturn(CARD2_FIRST)
			.willReturn(CARD1_FIRST);

		// when
		List<Card> newShoe = underTest.shuffle(shoe);
		
		// then
		assertThat(newShoe)
			.containsExactly(
					card(HEART, 3),
					card(SPADE, 1),
					card(CLUB, 10),
					card(DIAMOND, 6));
	}

	@Test
	public void shuffle_halfAndHalf_6cards() {
		// given
		shoeContains(
				card(HEART, 3),
				card(SPADE, 4),
				card(DIAMOND, 6),
				card(HEART, 5),
				card(DIAMOND, 7),
				card(CLUB, 10));
		given(random.nextBoolean())
			.willReturn(CARD2_FIRST)
			.willReturn(CARD1_FIRST)
			.willReturn(CARD2_FIRST);

		// when
		List<Card> newShoe = underTest.shuffle(shoe);
		
		// then
		assertThat(newShoe)
			.containsExactly(
					card(HEART, 3),
					card(HEART, 5),
					card(DIAMOND, 7),
					card(SPADE, 4),
					card(DIAMOND, 6),
					card(CLUB, 10)
					);
	}

	@Test
	public void shuffle_halfAndHalf_oddCards() {
		// given
		shoeContains(
				card(HEART, 3),
				card(SPADE, 4),
				card(DIAMOND, 6),
				card(HEART, 5),
				card(CLUB, 10));
		given(random.nextBoolean())
			.willReturn(CARD2_FIRST)
			.willReturn(CARD1_FIRST)
			.willReturn(CARD2_FIRST)
			;

		// when
		List<Card> newShoe = underTest.shuffle(shoe);
		
		// then
		assertThat(newShoe)
			.containsExactly(
					card(HEART, 3),
					card(DIAMOND, 6),
					card(HEART, 5),
					card(SPADE, 4),
					card(CLUB, 10)
					);
	}

	@Test
	public void shuffle_oneCard() {
		// given
		shoeContains(card(Suit.HEART, 3));
		
		// when
		List<Card> newShoe = underTest.shuffle(shoe);
		
		// then
		assertThat(newShoe)
			.containsExactly(card(Suit.HEART, 3));
	}

}
