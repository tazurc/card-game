package ca.logmein.card.game.component.shuffler;

import static ca.logmein.card.game.constant.Suit.CLUB;
import static ca.logmein.card.game.constant.Suit.DIAMOND;
import static ca.logmein.card.game.constant.Suit.HEART;
import static ca.logmein.card.game.constant.Suit.SPADE;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.BDDMockito.given;

import java.util.ArrayList;
import java.util.List;

import org.assertj.core.api.ThrowableAssert.ThrowingCallable;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner.Silent;

import ca.logmein.card.game.entity.Card;

@RunWith(Silent.class)
public class OverTableShufflerTest extends ShufflerTest {

	private @InjectMocks OverTableShuffler underTest;

	@Test
	public void cardsIsNull() {
		// given
		shoe = null;

		// when
		ThrowingCallable shuffle = () -> underTest.shuffle(shoe);

		// then
		assertThatThrownBy(shuffle)
				.isInstanceOf(IllegalArgumentException.class)
				.hasMessage("cards cannot be null");
	}
	
	@Test
	public void cardsIsEmpty() {
		// given
		shoe = new ArrayList<>();
		
		// when
		List<Card> newShoe = underTest.shuffle(shoe);
		
		// then
		assertThat(newShoe).isEmpty();
	}
	
	@Test
	public void shuffle_halfAndHalf_4cards() {
		// given
		shoeContains(
				card(HEART, 3),
				card(DIAMOND, 6),
				card(SPADE, 1),
				card(CLUB, 10));
		given(random.nextInt(4))
			.willReturn(1);
		given(random.nextInt(3))
			.willReturn(2);
		given(random.nextInt(2))
			.willReturn(0);
		given(random.nextInt(1))
			.willThrow(IllegalArgumentException.class);

		// when
		List<Card> newShoe = underTest.shuffle(shoe);
		
		// then
		assertThat(newShoe)
			.containsExactly(
					card(DIAMOND, 6),
					card(CLUB, 10),
					card(HEART, 3),
					card(SPADE, 1)
			);
	}

}
