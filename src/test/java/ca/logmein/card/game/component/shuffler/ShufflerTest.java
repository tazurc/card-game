package ca.logmein.card.game.component.shuffler;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.mockito.Mock;

import ca.logmein.card.game.constant.Suit;
import ca.logmein.card.game.entity.Card;

public abstract class ShufflerTest {

	@Mock
	protected Random random;
	protected List<Card> shoe = new ArrayList<>();

	protected void shoeContains(Card...cards) {
		for (Card card : cards) {
			this.shoe.add(card);
		}
	}

	protected Card card(Suit suit, int cardValue) {
		Card card = new Card();
		card.setSuit(suit);
		card.setValue(cardValue);
		return card;
	}

}